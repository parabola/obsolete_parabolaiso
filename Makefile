V=6

INSTALL_SRC_DIR=parabolaiso/initcpio/install
HOOKS_SRC_DIR=parabolaiso/initcpio/hooks
SCRIPT_SRC_DIR=parabolaiso/initcpio/script

INSTALL_DIR=$(DESTDIR)/usr/lib/initcpio/install
HOOKS_DIR=$(DESTDIR)/usr/lib/initcpio/hooks
SCRIPT_DIR=$(DESTDIR)/usr/lib/initcpio
DOC_DIR=$(DESTDIR)/usr/share/doc/parabolaiso


all: install dist

install: install-program install-initcpio install-examples install-doc

install-program:
	install -D -m 755 parabolaiso/mkparabolaiso "$(DESTDIR)/usr/sbin/mkparabolaiso"

install-initcpio:
	install -d "$(SCRIPT_DIR)" "$(HOOKS_DIR)" "$(INSTALL_DIR)"
	install -m 755 -t "$(SCRIPT_DIR)" $(wildcard $(SCRIPT_SRC_DIR)/*)
	install -m 644 -t "$(HOOKS_DIR)" $(wildcard $(HOOKS_SRC_DIR)/*)
	install -m 644 -t "$(INSTALL_DIR)" $(wildcard $(INSTALL_SRC_DIR)/*)

install-examples:
	install -d -m 755 "$(DESTDIR)/usr/share/parabolaiso/"
	cp -a --no-preserve=ownership configs "$(DESTDIR)/usr/share/parabolaiso/"

install-doc:
	install -d "$(DOC_DIR)"
	install -m 644 -t "$(DOC_DIR)" $(wildcard docs/*)

uninstall: uninstall-program uninstall-initcpio uninstall-examples uninstall-doc

uninstall-program:
	rm "$(DESTDIR)/usr/sbin/mkparabolaiso"

uninstall-initcpio:
	$(foreach file,$(wildcard $(SCRIPT_SRC_DIR)/*),rm -r "$(subst $(SCRIPT_SRC_DIR),$(SCRIPT_DIR),$(file))";)
	$(foreach file,$(wildcard $(HOOKS_SRC_DIR)/*),rm -r "$(subst $(HOOKS_SRC_DIR),$(HOOKS_DIR),$(file))";)
	$(foreach file,$(wildcard $(INSTALL_SRC_DIR)/*),rm -r "$(subst $(INSTALL_SRC_DIR),$(INSTALL_DIR),$(file))";)

uninstall-examples:
	rm -rfd "$(DESTDIR)/usr/share/parabolaiso/configs"
	rm -d "$(DESTDIR)/usr/share/parabolaiso"

uninstall-doc:
	rm -rf "$(DOC_DIR)"

dist:
	git archive --format=tar --prefix=parabolaiso-$(V)/ v$(V) | gzip -9 > parabolaiso-$(V).tar.gz
	gpg --detach-sign --use-agent parabolaiso-$(V).tar.gz

.PHONY: install install-program install-initcpio install-examples install-doc dist uninstall uninstall-program uninstall-initcpio uninstall-examples uninstall-doc
